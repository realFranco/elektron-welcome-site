# elektron-welcome-site

![python3](https://img.shields.io/badge/python-3.8.x-blue.svg?style=for-the-badge&logo=appveyor )

Static web site, powered by Flask + Nginx + Gunicorn

To start the project as local host use

```shell

sudo apt-get install python3-venv

python3 -m venv env

source env/bin/activate

pip3 install -r requirements.txt

python3 app/main.py
```

-- Comming soon will be included here the recipe collected from Digital Ocean
that will explaing how to deploy the service using Gunicorn + NGINX.
