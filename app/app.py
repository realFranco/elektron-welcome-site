"""
Date: June 29, 2021

Description: Entry point for the Elektron welcome page.

Filename: main.py
"""

from flask import Flask, render_template

app = Flask(__name__)

@app.route("/")
def server_static_welcome_page():
    return render_template('index.html')

if __name__ == "__main__":
    # app.run(host='0.0.0.0') # By default, the deploy was set it on port 5000
    app.run(port='8000')
